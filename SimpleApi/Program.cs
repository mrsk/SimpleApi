using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using System.Text.Json;




public static class Program
{

    public static void Main(string[] args)
    {
        StartBackgroundJob(args);
        ListenHttpApi(args);
    }
    public static void ListenHttpApi(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        var app = builder.Build();

        app.MapPost("/api/endpoint", async context =>
        {
            using var reader = new StreamReader(context.Request.Body);
            var requestBody = await reader.ReadToEndAsync();

            // Your JSON processing logic goes here
            var jsonDocument = JsonDocument.Parse(requestBody);
            var wat = jsonDocument.RootElement.GetProperty("wat");
            Console.WriteLine(wat);

            await context.Response.WriteAsJsonAsync(jsonDocument);
        });

        app.Run();
    }

    public static void StartBackgroundJob(string[] args)
    {
        Thread thread = new Thread(BackgroundJob)
        {
            IsBackground = true
        };
        thread.Start();
    }

    public static void BackgroundJob()
    {
        while (true)
        {
            Console.WriteLine("doing task");
            Thread.Sleep(1000);
        }
    }

}






